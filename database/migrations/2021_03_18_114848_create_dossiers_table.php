<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dossiers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('investisseur_id');
            $table->unsignedBigInteger('programme_id');
            $table->timestamps();

            $table->unique(['investisseur_id', 'programme_id']);

            $table->foreign('investisseur_id')->references('id')->on('investisseurs');
            $table->foreign('programme_id')->references('id')->on('programme');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dossiers');
    }
}
