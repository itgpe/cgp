<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AjoutMariageFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('conjoints', function (Blueprint $table) {
            $table->string('date_mariage')->nullable();
            $table->string('lieu_mariage')->nullable();
            $table->string('contrat_mariage')->nullable();
            $table->string('avocat_mariage')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('dossiers', function (Blueprint $table) {
            $table->dropColumn(['date_mariage', 'lieu_mariage', 'contrat_mariage', 'avocat_mariage']);
        });
    }
}
