<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeInvestisseurNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investisseurs', function (Blueprint $table) {
            $table->date('date_naissance')->nullable()->change();
            $table->string('lieu_naissance')->nullable()->change();
            $table->string('nationalite')->nullable()->change();
            $table->string('portable')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->text('adresse')->nullable()->change();
            $table->string('cp')->nullable()->change();
            $table->string('ville')->nullable()->change();
            $table->string('pays')->nullable()->change();

        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investisseurs', function (Blueprint $table) {
            $table->date('date_naissance')->change();
            $table->string('lieu_naissance')->change();
            $table->string('nationalite')->change();
            $table->string('portable')->change();
            $table->string('email')->change();
            $table->text('adresse')->change();
            $table->string('cp')->change();
            $table->string('ville')->change();
            $table->string('pays')->change();
        });
    }
}
