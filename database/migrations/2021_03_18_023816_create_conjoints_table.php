<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConjointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conjoints', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('nom_jeune_fille')->nullable();
            $table->string('prenom');
            $table->date('date_naissance');
            $table->text('adresse');
            $table->string('cp');
            $table->string('ville');
            $table->string('pays');
            $table->string('nationalite');
            $table->unsignedBigInteger('investisseur_id');
            $table->timestamps();

            $table->foreign('investisseur_id')->references('id')->on('investisseurs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conjoints');
    }
}
