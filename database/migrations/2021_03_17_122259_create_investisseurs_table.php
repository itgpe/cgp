<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestisseursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investisseurs', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prenom');
            $table->date('date_naissance');
            $table->string('lieu_naissance');
            $table->string('nationalite');
            $table->string('portable');
            $table->string('email');
            $table->text('adresse');
            $table->string('cp');
            $table->string('ville');
            $table->string('pays');
            $table->string('statut_matrimonial')->nullable();
            //$table->json('mariage_infos')->nullable();
            
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('conjoint_id')->nullable();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('conjoint_id')->references('id')->on('conjoints')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investisseurs');
    }
}
