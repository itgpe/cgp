<?php

namespace Database\Factories;

use App\Models\Investisseur;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvestisseurFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Investisseur::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
        return [
            'nom' => $this->faker->lastName,
            'prenom' => $this->faker->firstName,
            'date_naissance' => $this->faker->dateTimeThisCentury('-50 years'),
            'lieu_naissance' => $this->faker->city,
            'nationalite' => $this->faker->country,
            'portable' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->safeEmail,
            'adresse' => $this->faker->address,
            'cp' => $this->faker->postcode,
            'ville' => $this->faker->city,
            'pays' => $this->faker->country,
            'statut_matrimonial' => 'Célibataire',
            'user_id' => 1,
        ];
    }
}
