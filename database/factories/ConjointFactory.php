<?php

namespace Database\Factories;

use App\Models\Conjoint;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConjointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Conjoint::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
        return [
            'nom' => $this->faker->lastName,
            'nom_jeune_fille' => $this->faker->lastName,
            'prenom' => $this->faker->firstName,
            'date_naissance' => $this->faker->dateTimeThisCentury('-50 years'),
            'adresse' => $this->faker->address,
            'cp' => $this->faker->postcode,
            'ville' => $this->faker->city,
            'pays' => $this->faker->country,
            'nationalite' => $this->faker->country,
            'investisseur_id' => 1,
        ];
    }
}
