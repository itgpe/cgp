@extends('../layout')



@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection





@section ('content')

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0">Créer un conjoint</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary" href="/dossier/{{$dossier->id}}">
                                        <i class="mdi mdi-chevron-left mr-1"></i>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <form method="POST" action="/conjoints" />
        @csrf
        <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Conjoint</h2>
                        <p class="text-muted">Informations personnelles du conjoint.</p>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                        
                        <div class="form-group row">
                            <label for="nom" class="col-sm-3 col-form-label">Nom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ old('nom') }}" id="nom" name="nom">
                            </div>
                        </div>
                       
                       
                        <div class="form-group row">
                            <label for="nom_jeune_fille" class="col-sm-3 col-form-label">Nom de jeune fille </label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ old('nom_jeune_fille') }}" id="nom_jeune_fille" name="nom_jeune_fille">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="prenom" class="col-sm-3 col-form-label">Prénom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ old('prenom') }}" id="prenom" name="prenom">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="date_naissance" class="col-sm-3 col-form-label">Date de naissance</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="{{ old('date_naissance') }}" id="date_naissance" name="date_naissance">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nationalité</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="nationalite">
                                    <option>Large select</option>
                                    <option>Small select</option>
                                </select>
                            </div>
                        </div>
                    
                    
                    
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Adresse</label>
                            <div class="col-sm-9">
                                <textarea name="adresse" id="" cols="30" rows="5" class="form-control">{{ old('adresse') }}</textarea>
                            </div>
                        </div>


                         <div class="form-group row">
                            <label for="cp" class="col-sm-3 col-form-label">Code postal</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ old('cp') }}" id="cp" name="cp">
                            </div>
                        </div>
                         
                         
                         
                         <div class="form-group row">
                            <label for="ville" class="col-sm-3 col-form-label">Ville</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ old('ville') }}" id="ville" name="ville">
                            </div>
                        </div>



                         <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Pays</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="pays">
                                    <option>Large select</option>
                                    <option>Small select</option>
                                </select>
                            </div>
                        </div>
                        
                        
                    <h2>Informations mariage / PACS</h2>
                    
                     <div class="form-group row">
                            <label for="date_mariage" class="col-sm-3 col-form-label">Date de mariage</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="{{ old('date_mariage') }}" id="date_mariage" name="date_mariage">
                            </div>
                        </div>


                    <div class="form-group row">
                        <label for="lieu_mariage" class="col-sm-3 col-form-label">Mairie de mariage</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" value="{{ old('lieu_mariage') }}" id="lieu_mariage" name="lieu_mariage">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="contrat_mariage" class="col-sm-3 col-form-label">Contrat</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" value="{{ old('contrat_mariage') }}" id="contrat_mariage" name="contrat_mariage">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="avocat_mariage" class="col-sm-3 col-form-label">Déposé chez Maître</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" value="{{ old('avocat_mariage') }}" id="avocat_mariage" name="avocat_mariage">
                        </div>
                    </div>





                <div class="row">
                    <div class="col-12 text-right">
                    <input type="hidden" name="dossier_id" value="{{$dossier->id}}">
                    <input type="hidden" name="investisseur_id" value="{{$dossier->investisseur_id}}">
                    <input type="submit" class="btn btn-primary" value="Enregistrer">
                    </div>
                </div>

                    </div>
                </div>
            </div> <!-- end col -->
        </form>


    </div> <!-- end container-fluid -->
</div>
<!-- end wrapper -->
@endsection


@section('js')

<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>

<!-- App js -->
<script src="/assets/js/app.js"></script>


@endsection