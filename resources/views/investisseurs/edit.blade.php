@extends('layout')



@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection





@section ('content')

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h1 class="page-title m-0">Modifier une fiche d'investisseur</h1>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">


                                <input type="submit" class="btn btn-primary" value="Enregistrer">

                                <a class="btn btn-secondary" href="/investisseurs">
                                    <i class="mdi mdi-eye mr-1"></i> Voir la fiche
                                </a>


                                <a class="btn btn-outline-primary" href="/investisseurs">
                                    <i class="mdi mdi-chevron-left mr-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <form method="POST" action="/investisseurs/edit/{{$investisseur->id}}" />
        @csrf

        @method('PUT')

        <div class="row">
            <div class="col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Investisseur</h2>
                        <p class="text-muted">Informations personnelles de l'investisseur.</p>
                        <div class="form-group row">
                            <label for="nom" class="col-sm-3 col-form-label">Nom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->nom }}" id="nom" name="nom">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="prenom" class="col-sm-3 col-form-label">Prénom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->prenom }}" id="prenom" name="prenom">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->email }}" id="email" name="email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-date-input" class="col-sm-3 col-form-label">Date de naissance</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="{{ $investisseur->date_de_naissance }}" id="example-date-input">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Select</label>
                            <div class="col-sm-9">
                                <select class="form-control">
                                    <option>Select</option>
                                    <option>Large select</option>
                                    <option>Small select</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div> <!-- end col -->


            <div class="col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Opération</h2>
                        <p class="text-muted">Détaillez les informations financières de l'opération.</p>

                        <div class="form-group row">
                            <label for="c_nom" class="col-sm-3 col-form-label">Montant de la souscription</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->souscription }}" id="c_nom" name="c_nom">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="c_reduction_impots" class="col-sm-3 col-form-label">Réduction d'impôts</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->souscription }}" id="c_reduction_impots" name="c_reduction_impots">
                            </div>
                        </div>


                    </div>
                </div>


                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Conjoint</h2>
                        <p class="text-muted">Veuillez compléter les informations liées au conjoint.</p>

                        <div class="form-group row">
                            <label for="c_nom" class="col-sm-3 col-form-label">Nom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->souscription }}" id="c_nom" name="c_nom">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="c_nom_jeune_fille" class="col-sm-3 col-form-label">Nom de jeune fille</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->souscription }}" id="c_nom_jeune_fille" name="c_nom_jeune_fille">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="c_prenom" class="col-sm-3 col-form-label">Prénom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->souscription }}" id="c_prenom" name="c_prenom">
                            </div>
                        </div>


                    </div>
                </div>

            </div> <!-- end row -->
            </form>


        </div> <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->
    @endsection


    @section('js')

    <!-- jQuery  -->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/modernizr.min.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>

    <!-- App js -->
    <script src="/assets/js/app.js"></script>


    @endsection