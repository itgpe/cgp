@extends('../layout')



@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection





@section ('content')

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h1 class="page-title m-0">Consulter une fiche d'investisseur</h1>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">

                                <a class="btn btn-primary" href="/investisseurs">
                                    <i class="mdi mdi-edit mr-1"></i> Modifier
                                </a>


                                <a class="btn btn-outline-primary" href="/investisseurs">
                                    <i class="mdi mdi-chevron-left mr-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <form method="POST" action="/investisseurs/edit/{{$investisseur->id}}" />
        @csrf

        @method('PUT')

        <div class="row">
            <div class="col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Investisseur</h2>
                        <p class="text-muted">Informations personnelles de l'investisseur.</p>

                        <table class="table">
                            <tr>
                                <td>Nom</td>
                                <td>{{$investisseur->nom}}</td>
                            </tr>
                            <tr>
                                <td>Prénom</td>
                                <td>{{$investisseur->prenom}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{$investisseur->email}}</td>
                            </tr>
                            <tr>
                                <td>Téléphone</td>
                                <td>{{$investisseur->phone}}</td>
                            </tr>
                            <tr>
                                <td>Date de naissance</td>
                                <td>{{$investisseur->date_de_naissance}}</td>
                            </tr>
                            <tr>
                                <td>Lieu de naissance</td>
                                <td>{{$investisseur->lieu_de_naissance}}</td>
                            </tr>
                            <tr>
                                <td>Adresse</td>
                                <td>{{$investisseur->adresse}}
                                    <br /> {{$investisseur->cp}}
                                    <br /> {{$investisseur->pays}}
                                </td>
                            </tr>
                            <tr>
                                <td>Statut matrimonial</td>
                                <td>{{$investisseur->statut_matrimonial}}</td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div> <!-- end col -->


            <div class="col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Opération</h2>
                        <p class="text-muted">Informations financières de l'opération.</p>

                        <table class="table">
                            <tr>
                                <td>Montant de la souscription</td>
                                <td>{{$investisseur->montant_souscription}}</td>
                            </tr>
                            <tr>
                                <td>Réduction d'impôts</td>
                                <td>{{$investisseur->reduction_impots}}</td>
                            </tr>
                        </table>

                    </div>
                </div>


                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Conjoint</h2>
                        <p class="text-muted">Informations liées au conjoint.</p>

                        <table class="table">
                            <tr>
                                <td>Nom</td>
                                <td>{{$conjoint->nom}}</td>
                            </tr>
                            <tr>
                                <td>Nom< de jeune fille/td>
                                <td>{{$conjoint->nom_jeune_fille}}</td>
                            </tr>
                            <tr>
                                <td>Prénom</td>
                                <td>{{$conjoint->prenom}}</td>
                            </tr>
                            <td>{{$conjoint->adresse}}
                                <br /> {{$conjoint->cp}}
                                <br /> {{$conjoint->pays}}
                            </td>
                            <tr>
                                <td>Nationalité</td>
                                <td>{{$conjoint->nationalite}}</td>
                            </tr>
                        </table>

                    </div>
                </div>

            </div> <!-- end row -->
            </form>


        </div> <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->
    @endsection


    @section('js')

    <!-- jQuery  -->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/modernizr.min.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>

    <!-- App js -->
    <script src="/assets/js/app.js"></script>


    @endsection