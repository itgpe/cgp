@extends('layout')



@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection





@section ('content')



<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h1 class="page-title m-0">Editer un dossier - {{$investisseur->nom}} {{$investisseur->prenom}}</h1>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">

                                <a class="btn btn-primary" href="/dossiers/{{$dossier->id}}">
                                    <i class="mdi mdi-eye mr-1"></i> Voir le dossier avant signature
                                </a>


                                <a class="btn btn-outline-primary" href="/dossiers">
                                    <i class="mdi mdi-chevron-left mr-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-6">

     <!-- investisseur form -->       
<form method="POST" action="/investisseurs/edit/{{$investisseur->id}}" />
@csrf

@method('PUT')
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Investisseur</h2>
                        <p class="text-muted">Informations personnelles de l'investisseur.</p>
                        <div class="form-group row">
                            <label for="nom" class="col-sm-3 col-form-label">Nom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->nom }}" id="nom" name="nom">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="prenom" class="col-sm-3 col-form-label">Prénom</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->prenom }}" id="prenom" name="prenom">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="date_naissance" class="col-sm-3 col-form-label">Date de naissance</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="{{ $investisseur->date_naissance }}" id="date_naissance" name="date_naissance">
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="lieu_naissance" class="col-sm-3 col-form-label">Lieu de naissance</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->lieu_naissance }}" id="lieu_naissance" name="lieu_naissance">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nationalité</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="nationalite">
                                    <option>Large select</option>
                                    <option>Small select</option>
                                </select>
                            </div>
                        </div>



                          <div class="form-group row">
                            <label for="portable" class="col-sm-3 col-form-label">Portable</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->portable }}" id="lieu_naissance" name="portable">
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="email" value="{{ $investisseur->email }}" id="email" name="email">
                            </div>
                        </div>
                    
                    
                    
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Adresse</label>
                            <div class="col-sm-9">
                                <textarea name="adresse" id="" cols="30" rows="5" class="form-control">{{ $investisseur->adresse }}</textarea>
                            </div>
                        </div>


                         <div class="form-group row">
                            <label for="cp" class="col-sm-3 col-form-label">Code postal</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->cp }}" id="cp" name="cp">
                            </div>
                        </div>
                         
                         
                         
                         <div class="form-group row">
                            <label for="ville" class="col-sm-3 col-form-label">Ville</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $investisseur->ville }}" id="ville" name="ville">
                            </div>
                        </div>



                         <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Pays</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="pays">
                                    <option>Large select</option>
                                    <option>Small select</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        
                         <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Statut matrimonial</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="statut_matrimonial">
                                    <option>Célibataire</option>
                                    <option>Marié(e)</option>
                                    <option>Pacsé(e)</option>
                                    <option>Divorcé(e)</option>
                                    <option>Veuf(ve)</option>
                                </select>
                            </div>
                        </div>


                <div class="row">
                    <div class="col-12 text-right">
                    <input type="hidden" name="dossier_id" value="{{$dossier->id}}">
                    <input type="submit" class="btn btn-primary" value="Enregistrer">
                    </div>
                </div>

                    </div>
                </div>
            </div> <!-- end col -->
    </form>
<!-- end investisseur form -->




            <div class="col-6">
                 <!-- dossier form -->       
<form method="POST" action="/dossiers/edit/{{$dossier->id}}" />
@csrf
@method('PUT')
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Opération</h2>
                        <p class="text-muted">Détaillez les informations financières de l'opération.</p>

                        <div class="form-group row">
                            <label for="montant_souscription" class="col-sm-3 col-form-label">Montant de la souscription</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $dossier->montant_souscription }}" id="montant_souscription" name="montant_souscription">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="reduction_impots" class="col-sm-3 col-form-label">Réduction d'impôts attendue</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $dossier->reduction_impots }}" id="reduction_impots" name="reduction_impots">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Programme</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="programme_id">
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                        </div>

                
                <div class="row">
                    <div class="col-12 text-right">
                    <input type="hidden" name="dossier_id" value="{{$dossier->id}}">
                    <input type="hidden" name="investisseur_id" value="{{$investisseur->id}}">
                    <input type="submit" class="btn btn-primary" value="Enregistrer">
                    </div>
                </div>




                    </div>
                </div>
</form>
<!-- end dossier form -->



                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Conjoint</h2>
                        <p class="text-muted">Informations liées au conjoint.</p>

                        @if(isset($conjoint))
                        <table class="table">
                            <tr>
                                <td>Nom</td>
                                <td>{{$conjoint->nom}}</td>
                            </tr>
                            
                            <tr>
                                <td>Nom de jeune fille/td>
                                <td>{{$conjoint->nom_jeune_fille}}</td>
                            </tr>
                            
                            <tr>
                                <td>Prénom</td>
                                <td>{{$conjoint->prenom}}</td>
                            </tr>
                            
                            <tr>
                                <td>Date de naissance</td>
                                <td>{{$conjoint->date_naissance}}</td>
                            </tr>

                            <tr>
                                <td>Adresse</td>
                                <td>{{$conjoint->adresse}}
                                    <br /> {{$conjoint->cp}}
                                    <br /> {{$conjoint->ville}}
                                    <br /> {{$conjoint->pays}}
                                </td>
                            </tr>

                            <tr>
                                <td>Nationalité</td>
                                <td>{{$conjoint->nationalite}}</td>
                            </tr>
                            
                        </table>
                        
                        <p class="text-right">
                            <a href="/conjoint/delete/{$conjoint->id}" class="btn btn-outline-danger">Supprimer</a>
                        </p>


                        @else 
                        <p>
                            Aucun conjoint. <a href="/conjoints/create/{{$dossier->id}}">Ajouter</a>
                        </p>
                        
                        @endif

                    </div>
                </div>

            </div> <!-- end row -->
        


        </div> <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->
    
    
    
    @endsection


    @section('js')

    <!-- jQuery  -->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/modernizr.min.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>

    <!-- App js -->
    <script src="/assets/js/app.js"></script>


    @endsection