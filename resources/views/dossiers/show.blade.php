@extends('../layout')



@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection





@section ('content')

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h1 class="page-title m-0">Consulter un dossier - {{$investisseur->nom}} {{$investisseur->prenom}}</h1>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">

                                <a class="btn btn-primary" href="/dossiers/edit/{{$dossier->id}}">
                                    <i class="mdi mdi-edit mr-1"></i> Modifier
                                </a>


                                <a class="btn btn-outline-primary" href="/dossiers">
                                    <i class="mdi mdi-chevron-left mr-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Investisseur</h2>
                        <p class="text-muted">Informations personnelles de l'investisseur.</p>

                        <table class="table">
                            <tr>
                                <td>Nom</td>
                                <td><b>{{$investisseur->nom}}</b></td>
                            </tr>
                            <tr>
                                <td>Prénom</td>
                                <td><b>{{$investisseur->prenom}}</b></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><b>{{$investisseur->email}}</b></td>
                            </tr>
                            <tr>
                                <td>Téléphone</td>
                                <td><b>{{$investisseur->portable}}</b></td>
                            </tr>
                            <tr>
                                <td>Date de naissance</td>
                                <td><b>{{$investisseur->date_naissance}}</b></td>
                            </tr>
                            <tr>
                                <td>Lieu de naissance</td>
                                <td><b>{{$investisseur->lieu_naissance}}</b></td>
                            </tr>
                            <tr>
                                <td>Adresse</td>
                                <td><b>{{$investisseur->adresse}}
                                    <br /> {{$investisseur->cp}}
                                    <br /> {{$investisseur->pays}}</b>
                                </td>
                            </tr>
                            <tr>
                                <td>Statut matrimonial</td>
                                <td><b>{{$investisseur->statut_matrimonial}}</b></td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div> <!-- end col -->


            <div class="col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Opération</h2>
                        <p class="text-muted">Informations financières de l'opération.</p>

                        <table class="table">
                            <tr>
                                <td>Montant de la souscription</td>
                                <td><b>{{$dossier->montant_souscription}} EUR</b></td>
                            </tr>
                            <tr>
                                <td>Réduction d'impôts</td>
                                <td><b>{{$dossier->reduction_impots}} EUR</b></td>
                            </tr>
                        </table>

                    </div>
                </div>


                <div class="card m-b-30">
                    <div class="card-body">
                        <h2 class="mt-0 header-title">Conjoint</h2>
                        <p class="text-muted">Informations liées au conjoint.</p>

                         @if(isset($conjoint))
                        <table class="table">
                            <tr>
                                <td>Nom</td>
                                <td><b>{{$conjoint->nom}}</b></td>
                            </tr>
                            
                            <tr>
                                <td>Nom< de jeune fille/td>
                                <td><b>{{$conjoint->nom_jeune_fille}}</b></td>
                            </tr>
                            
                            <tr>
                                <td>Prénom</td>
                                <td><b>{{$conjoint->prenom}}</b></td>
                            </tr>
                            
                            <tr>
                                <td>Date de naissance</td>
                                <td><b>{{$conjoint->date_naissance}}</b></td>
                            </tr>

                            <tr>
                                <td>Adresse</td>
                                <td><b>{{$conjoint->adresse}}
                                    <br /> {{$conjoint->cp}}
                                    <br /> {{$conjoint->ville}}
                                    <br /> {{$conjoint->pays}}</b>
                                </td>
                            </tr>

                            <tr>
                                <td>Nationalité</td>
                                <td><b>{{$conjoint->nationalite}}</b></td>
                            </tr>
                           
                           
                            <tr>
                                <td>Date de mariage / Pacs</td>
                                <td><b>{{$conjoint->date_mariage}}</b></td>
                            </tr>
                            <tr>
                                <td>Lieu de mariage</td>
                                <td><b>{{$conjoint->lieu_mariage}}</b></td>
                            </tr>
                            <tr>
                                <td>Contrat</td>
                                <td><b>{{$conjoint->contrat_mariage}}</b></td>
                            </tr>
                            <tr>
                                <td>Déposé chez Maître</td>
                                <td><b>{{$conjoint->avocat_mariage}}</b></td>
                            </tr>
                            
                        </table>



                        @else 
                        <p>
                            Aucun conjoint.
                        </p>

                        @endif

                    </div>
                </div>

            </div> <!-- end row -->


        </div> <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->
    @endsection


    @section('js')

    <!-- jQuery  -->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/modernizr.min.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>

    <!-- App js -->
    <script src="/assets/js/app.js"></script>


    @endsection