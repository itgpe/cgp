@extends('../layout')



@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection





@section ('content')

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0">Créer un dossier</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary" href="/dossiers">
                                        <i class="mdi mdi-chevron-left mr-1"></i> Annuler
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <form method="POST" action="/dossiers" />
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="nom" class="col-sm-2 col-form-label">Nom</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="" id="nom" name="nom">
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label for="prenom" class="col-sm-2 col-form-label">Prénom</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="" id="prenom" name="prenom">
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label for="prenom" class="col-sm-2 col-form-label">Montant de la souscription</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="" id="montant_souscription" name="montant_souscription">
                            </div>
                        </div>
                       
                    

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Programme</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="programme_id">
                                    <option value="1">Primavera 2021</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                            <p class="text-right">
                                <input type="submit" class="btn btn-primary">
                            </p>
                        </div>
                        </div>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        </form>


    </div> <!-- end container-fluid -->
</div>
<!-- end wrapper -->
@endsection


@section('js')

<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>

<!-- App js -->
<script src="/assets/js/app.js"></script>


@endsection