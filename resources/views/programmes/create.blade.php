@extends('../layout')


@section('links')
<!-- App css -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
@endsection



@section ('content')

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0">Créer un programme</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="float-right d-none d-md-block">
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary" href="/programmes">
                                        <i class="mdi mdi-chevron-left mr-1"></i> Retour à la liste
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <form method="POST" action="/programmes" />
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="nom" class="col-sm-2 col-form-label">Nom</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="Primavera 2021" id="nom" name="nom">
                            </div>
                        </div>
                      
                       
                        <div class="form-group row">
                            <label for="montant" class="col-sm-2 col-form-label">Montant</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="150000" id="montant" name="montant">
                            </div>
                        </div>


                       

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sélectionnez un CGP </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="user_id">
                                    <option></option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                            <p class="text-right">
                                <input type="submit" class="btn btn-primary">
                            </p>
                        </div>
                        </div>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
        </form>


    </div> <!-- end container-fluid -->
</div>
<!-- end wrapper -->
@endsection



@section('js')

<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>

<!-- App js -->
<script src="/assets/js/app.js"></script>


@endsection

