<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dossier;
use App\Models\Investisseur;
use App\Models\Conjoint;
use App\Models\User;


class DossiersController extends Controller
{

    public function index(){
        $investisseurs = Investisseur::all();
        return view('dossiers/list', [
        'investisseurs' => $investisseurs
        ]);
    }
    
    
    
    public function create(){
        return view('dossiers/create');
    }
   
   

     public function show(Dossier $dossier)
    {
    
       $investisseur = Investisseur::find($dossier->investisseur_id);
       $dossier = Dossier::find($dossier->id);
       $conjoint = Conjoint::find($investisseur->conjoint_id);

        return view ('dossiers/show', [
            'investisseur' => $investisseur,
            'dossier' =>$dossier,
            'conjoint' => $conjoint
        ] );


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Investisseur  $investisseur
     * @return \Illuminate\Http\Response
     */
    public function edit(Dossier $dossier)
    {
       $investisseur = Investisseur::find($dossier->investisseur_id);
       $dossier = Dossier::find($dossier->id);
       $conjoint = Conjoint::find($investisseur->conjoint_id);

      // return $dossier;
        return view ('dossiers/edit', [
            'investisseur' => $investisseur,
            'dossier' =>$dossier,
            'conjoint' => $conjoint
        ] );
    }


   
    public function store(Request $request){

        DB::table('investisseurs')->insert([
                'nom' => request('nom'),
                'prenom' => request('prenom'),
                'user_id' => 1
            ]);
        $investisseur_id = DB::getPdo()->lastInsertId();
        
        DB::table('dossiers')->insert([
            'montant_souscription' => request('montant_souscription'),
            'investisseur_id' => $investisseur_id,
            'user_id' => 1,
            'programme_id' => request('programme_id'),
        ]);

        $dossier_id = DB::getPdo()->lastInsertId();

        return redirect('dossiers/edit/'.$dossier_id );
    }




        public function update( Dossier $dossier)
    {
        //return $investisseur; exit;
        $dossier_id = request('dossier_id');
        $dossier->update( 
                    request()->validate([
                            'montant_souscription' => 'required',
                            'reduction_impots' => 'required',
                            'programme_id' => 'required',
                            'investisseur_id' => 'required',
                            ] )
                            );
        return redirect('dossiers/edit/'.$dossier_id);
    }




}
