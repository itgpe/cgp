<?php

namespace App\Http\Controllers;

use App\Models\Investisseur;
use App\Models\Conjoint;
use Illuminate\Http\Request;

class InvestisseursController extends Controller
{



    public function index()
    {
        $investisseurs = Investisseur::all();
        return view('investisseurs/list', [
            'investisseurs' => $investisseurs
        ]);
    }
    



    public function create()
    {
        return view('investisseurs/create');
    }

  


    public function store(Request $request)
    {
        $result = Investisseur::create( $this->validateData() );

        return view('/investisseurs/show', [
            'investisseur' => $result
        ]);
    }


    protected function validateData(){
        return request()->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'date_naissance' => 'required',
            'lieu_naissance'  => 'required',
            'nationalite'  => 'required',
            'portable'  => 'required',
            'email'  => 'required',
            'adresse'  => 'required',
            'cp'  => 'required',
            'ville'  => 'required',
            'pays'  => 'required',
            'statut_matrimonial'  => 'required',
            'conjoint_id'  => '',
        ]);
    }

    
    public function show(Investisseur $investisseur)
    {

        $conjoint = new Conjoint;
        $conjoint->nom = "Grison";
        $conjoint->prenom = "Emilie";
    
        return view('investisseurs/show', [
            'investisseur' => $investisseur,
            'conjoint' => $conjoint
        ]);
    }

    
    public function edit(Investisseur $investisseur)
    {
        return view ('investisseurs/edit', [
            'investisseur' => $investisseur]
        );
    }

    
    public function update( Investisseur $investisseur)
    {
        //return $investisseur; exit;
        $dossier_id = request('dossier_id');
        $investisseur->update( $this->validateData() );
        return redirect('dossiers/edit/'.$dossier_id);
    }

   
    public function destroy(Investisseur $investisseur)
    {
        //
    }
}
