<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dossier;
use App\Models\Conjoint;

class ConjointsController extends Controller
{


    public function create(Dossier $dossier){
        $dossier = Dossier::find($dossier->id);
        return view('conjoints/create', [
            'dossier' => $dossier
        ]);
    }    
    
    
    
    
    public function store(Request $request)
    {
        $dossier_id = request('dossier_id');
        $dossier = Dossier::find($dossier_id);

        $result = Conjoint::create( $this->validateData() );
        $conjoint_id = DB::getPdo()->lastInsertId();

        DB::table('investisseurs')
            ->where('id', $dossier->investisseur_id)
            ->update(['conjoint_id' => $conjoint_id]);


        return redirect('dossiers/edit/'.$dossier_id );
    }


        protected function validateData(){
        return request()->validate([
            'nom' => 'required',
            'nom_jeune_fille' => '',
            'prenom' => 'required',
            'date_naissance' => 'required',
            'nationalite'  => 'required',
            'adresse'  => 'required',
            'cp'  => 'required',
            'ville'  => 'required',
            'pays'  => 'required',
            'investisseur_id'  => 'required',
            'date_mariage'  => '',
            'lieu_mariage'  => '',
            'contrat_mariage' => '',
            'avocat_mariage' => '',
        ]);
    }


}
