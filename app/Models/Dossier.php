<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dossier extends Model
{
    use HasFactory;

    protected $fillable = [
        'investisseur_id',
        'programme_id',
        'montant_souscription',
        'reduction_impots'
    ];


    public function investisseur(){
        return $this->hasOne(Investisseur::class);
    }
    
    public function conjoint(){
        return $this->hasOne(Conjoint::class);
    }
    
    
    public function user(){
        return $this->belongsTo(USer::class);
    }

    
}
