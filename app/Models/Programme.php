<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    use HasFactory;
    protected $fillable = [
            'nom', 
            'montant', 
            'user_id'
        ];


    public function user(){
        return $this->belongsTo(User::class);
    }


    public function investisseurs(){
        return $this->hasMany(Investisseur::class);
    }


}
