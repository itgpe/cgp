<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conjoint extends Model
{
    use HasFactory;
    protected $fillable = [
       'nom',
       'nom_jeune_fille',
       'prenom',
       'date_naissance',
       'adresse',
       'cp',
       'ville',
       'pays',
       'nationalite',
       'investisseur_id',
       'date_mariage',
       'lieu_mariage',
       'contrat_mariage',
       'avocat_mariage',
    ];

}
