<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investisseur extends Model
{
    use HasFactory;
    protected $fillable = [
       'nom',
       'prenom',
       'date_naissance',
       'lieu_naissance',
       'nationalite',
       'portable',
       'email',
       'adresse',
       'cp',
       'ville',
       'pays',
       'statut_matrimonial',
       'conjoint_id',
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }


    public function conjoints(){
        return $this->hasMany(Conjoint::class); //afin de gérer un même investisseur qui pourrait être re-marié
    }


    public function dossiers(){
        return $this->hasMany(Dossier::class);
    }


}
