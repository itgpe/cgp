<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/investisseurs', 'InvestisseursController@index');
//Route::post('/investisseurs', 'InvestisseursController@store');
//Route::get('/investisseurs/create', 'InvestisseursController@create');
//Route::get('/investisseurs/edit/{investisseur}', 'InvestisseursController@edit');
Route::get('/investisseurs/{investisseur}', 'InvestisseursController@show');
Route::put('/investisseurs/edit/{investisseur}', 'InvestisseursController@update');



Route::get('/dossiers', 'DossiersController@index');
Route::post('/dossiers', 'DossiersController@store');
Route::get('/dossiers/create', 'DossiersController@create');
Route::get('/dossiers/{dossier}', 'DossiersController@show');
Route::get('/dossiers/edit/{dossier}', 'DossiersController@edit');
Route::put('/dossiers/edit/{dossier}', 'DossiersController@update');



Route::get('/programmes', 'ProgrammesController@index');
Route::post('/programmes', 'ProgrammesController@store');
Route::get('/programmes/create', 'ProgrammesController@create');
Route::get('/programmes/edit/{programme}', 'ProgrammesController@edit');
Route::put('/programmes/edit/{programme}', 'ProgrammesController@update');

Route::get('/conjoints/create/{dossier}', 'ConjointsController@create');
Route::post('/conjoints', 'ConjointsController@store');